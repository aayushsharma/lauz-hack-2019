# Gesturely Automation

Tired of using traditional machines with multiple/non-user friendly keys/buttons? Why not communicate it with human gestures instead?

## Installation
#### Requirements
- `python3`
- `pip`
- `venv`

#### Installation
- Setup Virtual Enviroment: `` python3 -m pip venv .venv ``
- Initiate Virtual Environment: `` source .venv/bin/activate ``
- Install Dependencies: `` pip3 install -r requirements.txt ``

### Running
If you want to directly run from python: `python3 gesturely_automation.py`. 

If it does not work run `gesturely_automation.ipynb` file in `jupyter-notebook` (see https://jupyter.org/)

## Gestures
- 1 finger   for   &nbsp; &nbsp; &nbsp; Start Feeder
- 2 fingers  for  &nbsp; &nbsp; &nbsp; Load 100 Stacks
- 3 fingers  for  &nbsp; &nbsp; &nbsp; Speeding up machine
- 4 fingers  for  &nbsp; &nbsp; &nbsp; Stopping Feeder
- 5 fingers  for  &nbsp; &nbsp; &nbsp; Unblock, reset and load 100 stack when blocked
